Poquito
=======

## Deploy

```bash
# Spin up a server:
python manage.py provision
# Add project user:
pyinfra --user=root infra/inventory.py infra/user.py
# Install package dependencies:
pyinfra infra/inventory.py infra/packages.py
# Install pyenv:
pyinfra infra/inventory.py infra/pyenv.py
# Set environment:
pyinfra infra/inventory.py infra/environment.py
# Build application:
pyinfra infra/inventory.py infra/build.py
# Migrate database:
pyinfra infra/inventory.py infra/database.py
# Start server:
pyinfra infra/inventory.py infra/server.py
```


## Test

```bash
# Pytest runner integrated with `manage.py`:
python manage.py test

# ...which is equivalent to:
pytest --color=yes --no-header \
    # pytest-black
    --black \
    # pytest-xdist
    --dist=loadscope --maxprocesses=4 --numprocesses=logical
