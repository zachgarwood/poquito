import pytest


@pytest.fixture
def user(django_user_model, transactional_db):
    yield django_user_model.objects.create_user(email="test@example.com")


@pytest.fixture
def auth_client(client, user):
    client.force_login(user)
    yield client
