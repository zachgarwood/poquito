import pytest
from pytest_django.asserts import assertRedirects


pytestmark = pytest.mark.django_db

KEY_SLUG = "%F0%9F%94%91"  # 🔑


def test_login_form(client):
    response = client.get("/🔑/📧🔗/")
    assert response.status_code == 200, "The user can view the login form"
    response = client.post("/🔑/📧🔗/", {"email": "test@example.com"}, follow=True)
    assert response.status_code == 200, "The user can submit the login form"


def test_login_email(client, mailoutbox):
    email_address = "test@example.com"
    client.post("/🔑/📧🔗/", {"email": email_address})
    assert len(mailoutbox) == 1, "The login email was sent"
    login_email = mailoutbox[0]
    assert email_address in login_email.to, "The email was sent to the correct address"
    assert f"/{KEY_SLUG}/login/" in login_email.body, "The email contains the login link"
