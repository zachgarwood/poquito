import pytest
from poquito.models import Post
from pytest_django.asserts import assertRedirects


pytestmark = pytest.mark.django_db

POST_SLUG = "%F0%9F%A4%94"  # 🤔
NEW_SLUG = "🆕"  # 🆕
EDIT_SLUG = "✏"  # ✏


def test_all_post_list(client):
    response = client.get("/")
    assert response.status_code == 200, "An unauthed user can view the all post list"


def test_post_list_redirect(client, user):
    response = client.get(f"/{POST_SLUG}/")
    assertRedirects(response, "/%F0%9F%94%91/%F0%9F%93%A7%F0%9F%94%97/?next=%2F%25F0%259F%25A4%2594%2F")
    assert response.status_code == 302, "An unauthed user is redirected to the login page"


def test_post_list(auth_client):
    response = auth_client.get(f"/{POST_SLUG}/")
    assert response.status_code == 200, "An authorized user can view their post list"


def test_post_create(auth_client):
    response = auth_client.get(f"/{POST_SLUG}/{NEW_SLUG}/")
    assert response.status_code == 200, "The user can view the new post form"
    response = auth_client.post(f"/{POST_SLUG}/{NEW_SLUG}/", {"text": "🆕"}, follow=True)
    assert response.status_code == 200, "The user can submit the new post form"


def test_post_detail(auth_client, user):
    post = Post(text="🆕", user=user)
    response = auth_client.get(f"/{POST_SLUG}/{post.slug}/", follow=True)
    assert response.status_code == 404, "The user cannot view an unknown post"
    post.save()
    response = auth_client.get(f"/{POST_SLUG}/{post.slug}/", follow=True)
    assert response.status_code == 200, "The user can view the post detail"


def test_post_update(auth_client, user):
    original_post_text = "🆕"
    updated_post_text = "🤔"
    post = Post.objects.create(text=original_post_text, user=user)
    response = auth_client.get(f"/{POST_SLUG}/{post.slug}/{EDIT_SLUG}/")
    assert response.status_code == 200, "The user can view the edit post form"
    response = auth_client.post(f"/{POST_SLUG}/{post.slug}/{EDIT_SLUG}/", {"text": updated_post_text}, follow=True)
    assert response.status_code == 200, "The user can submit the edit post form"
    post.refresh_from_db()
    assert post.text == updated_post_text, "The post's text has been updated"


def test_post_delete(auth_client, user):
    post = Post.objects.create(text="🆕", user=user)
    response = auth_client.delete(f"/{POST_SLUG}/{post.slug}/🗑️/")
    assert response.status_code == 302, "The user can submit the deletion and is redirected to the post list"
    response = auth_client.get(f"/{POST_SLUG}/{post.slug}", follow=True)
    assert response.status_code == 404, "The user can no longer view the post"
    with pytest.raises(Post.DoesNotExist):
        post.refresh_from_db()
