"""
Adapted from:
https://pytest-django.readthedocs.io/en/latest/faq.html#how-can-i-use-manage-py-test-with-pytest-django
"""


class PytestTestRunner:
    """Runs pytest."""

    def __init__(self, verbosity=1, failfast=False, keepdb=False, **kwargs):
        self.verbosity = verbosity
        self.failfast = failfast
        self.keepdb = keepdb

    def run_tests(self, test_labels):
        import pytest

        argv = [
            "--black",
            "--color=yes",
            "--no-header",
            "--dist=loadscope",
            "--maxprocesses=4",
            "--numprocesses=logical",
            "--pythonwarnings=ignore",
        ]
        if self.verbosity == 0:
            argv.append("--quiet")
        if self.verbosity == 2:
            argv.append("--verbose")
        if self.verbosity == 3:
            argv.append("-vv")
        if self.failfast:
            argv.append("--exitfirst")
        if self.keepdb:
            argv.append("--reuse-db")

        argv.extend(test_labels)
        return pytest.main(argv)
