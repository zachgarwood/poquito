import pytest
from django.core.exceptions import ValidationError
from django.utils import timezone
from poquito.models import Post, validate_emoji


def test_set_audit_fields(user):
    post = Post(text="TEST", user=user)
    assert post.created_at is None

    before_created = timezone.now()
    post.set_audit_fields()
    assert post.created_at is not None
    assert post.created_at > before_created

    last_updated_at = post.updated_at
    post.set_audit_fields()
    assert post.updated_at != last_updated_at
    assert post.updated_at > last_updated_at


def test_validate_emoji():
    text_with_emoji = "🤏ℹ️"
    assert validate_emoji(text_with_emoji) is None

    text_with_non_emoji = "🤏ℹ️abc"
    with pytest.raises(ValidationError, match=r"🚫"):
        validate_emoji(text_with_non_emoji)

    text_with_whitespace = """ 🤏ℹ️
    """
    assert validate_emoji(text_with_whitespace) is None

    text_with_zero_width_space = "\u200d"
    assert validate_emoji(text_with_zero_width_space) is None
