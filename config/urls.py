"""poquito URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
"""

from django.conf import settings
from django.contrib import admin
from django.shortcuts import render
from django.urls import include, path

from poquito.views.post import AllPostList, PostCreate, PostDelete, PostDetail, PostList, PostUpdate
from poquito.views.post_card import PostCard
from poquito.views.reaction import ReactionCreate, ReactionDelete
from poquito.views.registration import CheckEmail


urlpatterns = [
    path("admin/", admin.site.urls),
    path("", AllPostList.as_view(), name="home"),
    path("🔑/", include("mailauth.urls")),
    path("🔑/📧🔗/", CheckEmail.as_view(), name="check-email"),
    path("🤔/", PostList.as_view(), name="post-list"),
    path("🤔/🆕/", PostCreate.as_view(), name="post-create"),
    path("🤔/<str:slug>/", PostDetail.as_view(), name="post-detail"),
    path("🤔/<str:slug>/card/", PostCard.as_view(), name="post-card"),
    path("🤔/<str:slug>/✏/", PostUpdate.as_view(), name="post-update"),
    path("🤔/<str:slug>/🗑️/", PostDelete.as_view(), name="post-delete"),
    path("🤔/<str:slug>/😶/🆕/", ReactionCreate.as_view(), name="reaction-create"),
    path("🤔/<str:slug>/😶/<int:pk>/🗑️", ReactionDelete.as_view(), name="reaction-delete"),
]
if settings.DEBUG:
    urlpatterns += [path("__debug__/", include("debug_toolbar.urls"))]
