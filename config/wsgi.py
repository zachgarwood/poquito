"""
WSGI config for poquito project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/3.1/howto/deployment/wsgi/
"""

import os
from pathlib import Path

from django.core.wsgi import get_wsgi_application

try:
    import dotenv

    dotenv_file = Path(__file__).resolve(strict=True).parent.parent / ".env"
    dotenv.read_dotenv(dotenv_file)
except ModuleNotFoundError:
    pass

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "config.settings")

application = get_wsgi_application()
