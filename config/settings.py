import os

from django.contrib.messages import constants as messages
from pathlib import Path
from urllib.parse import parse_qs, urlparse

# Build paths inside the project like this: BASE_DIR / 'subdir'.
BASE_DIR = Path(__file__).resolve(strict=True).parent.parent

SECRET_KEY = os.getenv("DJANGO_SECRET_KEY")

ENVIRONMENT = os.getenv("DJANGO_ENVIRONMENT")

DEBUG = ENVIRONMENT == "development"

ALLOWED_HOSTS = os.getenv("DJANGO_ALLOWED_HOSTS", "127.0.0.1,localhost").split(",")
INTERNAL_IPS = ["127.0.0.1"]

TEST_RUNNER = "tests.runner.PytestTestRunner"


# Application definition

INSTALLED_APPS = [
    "anymail",
    "poquito.apps.PoquitoConfig",
    "compressor",
    "django.contrib.admin",
    "django.contrib.auth",
    "django.contrib.contenttypes",
    "django.contrib.sessions",
    "django.contrib.messages",
    "django.contrib.staticfiles",
    "mailauth",
    "mailauth.contrib.admin",
    "mailauth.contrib.user",
]
if DEBUG:
    INSTALLED_APPS += ["debug_toolbar"]


MESSAGE_TAGS = {
    messages.INFO: "is-info",
    messages.SUCCESS: "is-success",
    messages.WARNING: "is-warning",
    messages.ERROR: "is-danger",
}

MIDDLEWARE = [
    "django.middleware.security.SecurityMiddleware",
    "django.contrib.sessions.middleware.SessionMiddleware",
    "django.middleware.common.CommonMiddleware",
    "django.middleware.csrf.CsrfViewMiddleware",
]
if DEBUG:
    MIDDLEWARE += ["debug_toolbar.middleware.DebugToolbarMiddleware"]
MIDDLEWARE += [
    "django.contrib.auth.middleware.AuthenticationMiddleware",
    "django.contrib.messages.middleware.MessageMiddleware",
    "django.middleware.clickjacking.XFrameOptionsMiddleware",
    "poquito.middleware.HtmxMessageMiddleware",
]

ROOT_URLCONF = "config.urls"

TEMPLATES = [
    {
        "BACKEND": "django.template.backends.django.DjangoTemplates",
        "DIRS": [],
        "APP_DIRS": True,
        "OPTIONS": {
            "context_processors": [
                "django.template.context_processors.debug",
                "django.template.context_processors.request",
                "django.contrib.auth.context_processors.auth",
                "django.contrib.messages.context_processors.messages",
            ],
        },
    },
]

WSGI_APPLICATION = "config.wsgi.application"


# Database
# https://docs.djangoproject.com/en/3.1/ref/settings/#databases

DATABASE_URL = os.getenv("DATABASE_URL")
if DATABASE_URL:
    database = urlparse(DATABASE_URL)
    DATABASES = {
        "default": {
            "ENGINE": "django.db.backends.postgresql",
            "NAME": database.path[1:],  # strip leading "/"
            "USER": database.username,
            "PASSWORD": database.password,
            "HOST": database.hostname,
            "PORT": database.port,
            "OPTIONS": parse_qs(database.query),
        }
    }
else:
    DATABASES = {"default": {"ENGINE": "django.db.backends.sqlite3", "NAME": "db.sqlite3"}}

DEFAULT_AUTO_FIELD = "django.db.models.AutoField"


# Auth
# https://django-mail-auth.readthedocs.io/en/stable/

AUTH_USER_MODEL = "mailauth_user.EmailUser"

AUTHENTICATION_BACKENDS = ["mailauth.backends.MailAuthBackend"]

LOGIN_REQUESTED_URL = "mailauth:login-success"

LOGIN_REDIRECT_URL = "home"

LOGIN_URL = "check-email"

LOGIN_URL_TIMEOUT = 60 * 15  # 15 minutes in seconds


# Internationalization
# https://docs.djangoproject.com/en/3.1/topics/i18n/

LANGUAGE_CODE = "en-us"

TIME_ZONE = "UTC"

USE_I18N = True

USE_TZ = True


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/3.1/howto/static-files/

STATIC_ROOT = BASE_DIR / "static"
STATIC_URL = "/static/"

STATICFILES_DIRS = [os.path.join(BASE_DIR, "vendor/")]
STATICFILES_FINDERS = [
    "django.contrib.staticfiles.finders.FileSystemFinder",
    "django.contrib.staticfiles.finders.AppDirectoriesFinder",
    "compressor.finders.CompressorFinder",
]

COMPRESS_PRECOMPILERS = [("text/x-scss", "django_libsass.SassCompiler")]


# Email

DEFAULT_FROM_EMAIL = "'🤏️ login' <login@poquito.zachgarwood.com>"

if ENVIRONMENT == "production":
    EMAIL_BACKEND = "anymail.backends.mailjet.EmailBackend"
else:
    EMAIL_BACKEND = "django.core.mail.backends.console.EmailBackend"

EMAIL_USE_TLS = True

EMAIL_PORT = 587

EMAIL_TIMEOUT = 10

ANYMAIL = {
    "MAILJET_API_KEY": os.getenv("MAILJET_API_KEY"),
    "MAILJET_SECRET_KEY": os.getenv("MAILJET_SECRET_KEY"),
}
