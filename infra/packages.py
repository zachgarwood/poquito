from pyinfra.operations import apt, server


apt_ppas = ["universe"]
for ppa in apt_ppas:
    apt.ppa(
        name=f"Ensure apt ppa {ppa}",
        src=ppa,
        present=True,
        _sudo=True,
    )

apt.update(
    name="Update apt repositiories",
    _sudo=True,
)
certbot_packages = ["python3-certbot-nginx"]
operational_packages = ["nginx", "sqlite3"]
pyenv_installer_packages = ["bash", "curl", "git"]
# https://github.com/pyenv/pyenv/wiki#suggested-build-environment
pyenv_packages = [
    "build-essential",
    "curl",
    "libbz2-dev",
    "libffi-dev",
    "liblzma-dev",
    "libncurses5-dev",
    "libreadline-dev",
    "libsqlite3-dev",
    "libssl-dev",
    "libxml2-dev",
    "libxmlsec1-dev",
    "llvm",
    "make",
    "python3-openssl",
    "tk-dev",
    "wget",
    "xz-utils",
    "zlib1g-dev",
]
apt.packages(
    name="Install apt packages",
    packages=(certbot_packages + operational_packages + pyenv_installer_packages + pyenv_packages),
    cache_time=360,
    latest=True,
    present=True,
    _sudo=True,
)
server.shell(
    name="Clean up after apt",
    commands="apt-get autoremove -y",
    _sudo=True,
)
