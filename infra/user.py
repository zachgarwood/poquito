from pathlib import Path

from pyinfra import host
from pyinfra.operations import files, server


user = host.data.project
groups = host.data.groups
public_key = Path().home().joinpath(".ssh", f"{user}.pub")

for group in groups + [user]:
    server.group(
        name=f"Ensure {group} group",
        group=group,
        present=True,
    )

server.user(
    name=f"Ensure {user} user",
    user=user,
    ensure_home=True,
    group=user,
    groups=groups,
    public_keys=[str(public_key)],
    shell="/bin/bash",
)

files.line(
    name=f"Ensure {user} user is a passwordless sudoer",
    path="/etc/sudoers",
    line=f"{user} .*",
    present=True,
    replace=f"{user} ALL=(ALL) NOPASSWD: ALL",
)

files.line(
    name=f"Ensure {user} login session mirrors non-login",
    path="/home/poquito/.bash_profile",
    line=". .bashrc",
    present=True,
)
