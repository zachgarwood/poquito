from pathlib import Path

from pyinfra import host
from pyinfra.facts.files import File
from pyinfra.facts.server import Command
from pyinfra.operations import files, server, systemd


home = Path("/home") / host.data.project
project_root = home / host.data.project_root
infrastructure_files = project_root / "infra/files"


if host.get_fact(File, "/var/run/reboot-required"):
    server.reboot(
        name="System requires restart",
        delay=10,
        interval=10,
        reboot_timeout=60,
        _sudo=True,
    )

socket = host.data.project + ".socket"
files.link(
    name="Configure Poquito socket",
    path=f"/etc/systemd/system/{socket}",
    target=str(infrastructure_files / socket),
    symbolic=True,
    _sudo=True,
)
server.service(
    name="Enable Poquito socket",
    service=socket,
    enabled=True,
    running=False,
    _sudo=True,
)

service = host.data.project + ".service"
files.link(
    name="Configure Poquito service",
    path=f"/etc/systemd/system/{service}",
    target=str(infrastructure_files / service),
    symbolic=True,
    _sudo=True,
)
systemd.daemon_reload(_sudo=True)
server.service(
    name="Restart Poquito service",
    service=service,
    enabled=True,
    restarted=True,
    running=True,
    _sudo=True,
)

nginx_config = str(infrastructure_files / "nginx.conf")
if "Certificate Name: poquito.zachgarwood.com" not in host.get_fact(Command, "sudo certbot -n certificates"):
    server.shell(
        name="Create certificates",
        commands=[
            "certbot -n --nginx --agree-tos" " --email=zachgarwood@gmail.com" " --domains=poquito.zachgarwood.com",
        ],
        _sudo=True,
    )
files.link(
    name="Configure nginx",
    path=f"/etc/nginx/sites-enabled/{host.data.project}.conf",
    target=nginx_config,
    present=True,
    symbolic=True,
    _sudo=True,
)
server.shell(
    name="Check nginx configuration",
    commands="nginx -t",
    _sudo=True,
)
server.service(
    name="Restart nginx service",
    service="nginx",
    enabled=True,
    restarted=True,
    running=True,
    _sudo=True,
)
