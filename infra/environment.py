from pyinfra.operations import files


# The environment variable file is read by the poquito.service
files.put(
    name="Ensure environment variable file",
    src="infra/files/.environment",
    dest="/home/poquito/.environment",
    assume_exists=False,
)
files.line(
    name="Ensure environment variables exported from file",
    path="/home/poquito/.bashrc",
    line=r".environment",
    replace="set -o allexport && source $HOME/.environment && set +o allexport",
    interpolate_variables=False,
)
