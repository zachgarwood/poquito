from pathlib import Path

from pyinfra import host
from pyinfra.operations import git, server


home = Path("/home") / host.data.project
project_root = home / host.data.project_root

git.config(
    name="Set git user.name",
    key="user.name",
    value="Poquito build script",
)
git.config(
    name="Set git user.email",
    key="user.email",
    value="login@poquito.zachgarwood.com",
)
git.repo(
    name="Download source",
    src="https://gitlab.com/zachgarwood/poquito.git",
    dest=str(project_root),
    branch="production",
)
server.shell(
    name="Install Python version",
    commands="cat .python-version | pyenv install --skip-existing --verbose",
    _chdir=str(project_root),
)
server.shell(
    name="Set global Python version",
    commands="pyenv global `cat .python-version`",
    _chdir=str(project_root),
)
server.shell(
    name="Rehash pyenv shims",
    commands="pyenv rehash",
    _chdir=str(project_root),
)
server.shell(
    name="Install operational requirements",
    commands=f"pip install --upgrade -r {project_root}/requirements_ops.txt",
    _chdir=str(project_root),
)
server.shell(
    name="Install requirements",
    commands="pip install -r requirements.txt",
    _chdir=str(project_root),
)
server.shell(
    name="Collect static assets",
    commands="python manage.py collectstatic --no-input",
    _chdir=host.data.project_root,
)
