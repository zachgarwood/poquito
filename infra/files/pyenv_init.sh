#!/usr/bin/env bash
export PATH="$HOME/.pyenv/bin:$PATH"

if command -v pyenv 1>/dev/null; then
    eval "$(pyenv init -)"
    eval "$(pyenv virtualenv-init -)"
fi
