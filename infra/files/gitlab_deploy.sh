#!/bin/bash -x
# Deploy from Gitlab CI server

apt-get update -y && apt-get install -y \
    bash \
    curl \
    git \
    openssh-client
eval $(ssh-agent -s)
chmod a-rwx,u=rw $SSH_PRIVATE_KEY  # chmod 700
ssh-add $SSH_PRIVATE_KEY
# ssh-add outputs the following, I don't know why, but it still works, so *shrug*:
# Error loading key "/builds/zachgarwood/poquito.tmp/SSH_PRIVATE_KEY": invalid format
mkdir -p ~/.ssh
chmod -R a-rwx,u=rwx ~/.ssh  # chmod 600
ssh-keyscan poquito.zachgarwood.com > ~/.ssh/known_hosts
chmod a=r,u=rw ~/.ssh/known_hosts  # chmod 644
python -m pip install -r requirements_ci.txt
pyinfra --version
pyinfra infra/inventory.py \
    -v \
    --key $SSH_PRIVATE_KEY \
    infra/build.py \
    infra/database.py \
    infra/server.py
