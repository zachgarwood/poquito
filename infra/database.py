from pathlib import Path

from pyinfra import host
from pyinfra.operations import server


home = Path("/home") / host.data.project
project_root = home / host.data.project_root

server.shell(
    name="Migrate database",
    commands="python manage.py migrate",
    _chdir=str(project_root),
)
