from pathlib import Path

from pyinfra import host
from pyinfra.facts.files import Directory
from pyinfra.operations import files, server


home = Path("/home") / host.data.project

# For more information: https://github.com/pyenv/pyenv-installer
pyenv_root = home / ".pyenv"
if not host.get_fact(Directory, str(pyenv_root)):
    name = "pyenv-installer"
    commit_hash = "dd3f7d0914c5b4a416ca71ffabdf2954f2021596"
    path = Path("/tmp") / name
    file_name = commit_hash + ".sh"
    installer = path / file_name
    installer_url = f"https://raw.githubusercontent.com/pyenv/{name}/{commit_hash}/bin/{name}"

    files.directory(
        name="Ensure pyenv download directory",
        path=str(path),
    )
    files.download(
        name="Download pyenv installer",
        src=installer_url,
        dest=str(installer),
        mode="700",
    )
    server.shell(
        name="Install pyenv",
        commands=[
            f"rm --force --recursive {pyenv_root}",
            f"PYENV_ROOT={pyenv_root} . {installer}",
        ],
        shell_executable="sh",
    )

pyenv_init_path = home / ".pyenv_init.sh"
files.put(
    name="Set pyenv initialization script",
    src="infra/files/pyenv_init.sh",
    dest=str(pyenv_init_path),
)
bash_profile_path = home / ".bash_profile"
files.line(
    name="Ensure pyenv initialization on shell startup",
    path=str(bash_profile_path),
    line=r".pyenv_init.sh",
    replace=". $HOME/.pyenv_init.sh",
    present=True,
    interpolate_variables=False,
)
