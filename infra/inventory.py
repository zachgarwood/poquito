production = [
    (
        "poquito.zachgarwood.com",
        {
            "groups": ["www-data"],
            "project": "poquito",
            "project_root": "src",
            "ssh_user": "poquito",
        },
    )
]
