from django.forms import ModelForm, Textarea, TextInput

from poquito.models import Post, Reaction


class PostForm(ModelForm):
    class Meta:
        model = Post
        fields = ("text",)
        widgets = {
            "text": Textarea(
                attrs={
                    "class": "textarea lines",
                    "cols": 8,
                    "rows": 8,
                    "wrap": "hard",
                }
            )
        }


class ReactionForm(ModelForm):
    class Meta:
        model = Reaction
        fields = ("text",)
        widgets = {
            "text": TextInput(
                attrs={
                    "class": "input is-size-7 reaction",
                }
            )
        }
