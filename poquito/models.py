import random
import regex as re

import emoji
from django.contrib.auth import get_user_model
from django.core.exceptions import ValidationError
from django.db.models import CASCADE, CharField, DateTimeField, ForeignKey, Model
from django.urls import reverse
from django.utils import timezone

CHARACTERS_PER_LINE = LINES_PER_POST = 8
POST_TOO_LONG_MESSAGE = "🚫 8️⃣✖️8️⃣"


def validate_emoji(text):
    """\u200d is a zero width whitespace"""
    text_without_whitespace = "".join(text.replace("\u200d", "").split())
    invalid_characters = emoji.replace_emoji(text_without_whitespace)
    if invalid_characters:
        message = "🚫 " + ", ".join(sorted(set(invalid_characters)))
        raise ValidationError(message)


def validate_linebreaks(text):
    lines = text.splitlines()
    if len(lines) > LINES_PER_POST:
        raise ValidationError(POST_TOO_LONG_MESSAGE)
    for index, line in enumerate(lines):
        emojis = [grapheme for grapheme in re.findall(r"\X", line) if emoji.is_emoji(grapheme)]
        if len(emojis) > CHARACTERS_PER_LINE:
            raise ValidationError(POST_TOO_LONG_MESSAGE)


class Post(Model):
    class Meta:
        ordering = ("-created_at",)

    text = CharField(max_length=255, validators=[validate_emoji, validate_linebreaks])
    created_at = DateTimeField(editable=False)
    updated_at = DateTimeField(editable=False)
    user = ForeignKey(get_user_model(), on_delete=CASCADE, related_name="posts")
    slug = CharField(default=None, max_length=8, unique=True, validators=[validate_emoji])

    def get_absolute_url(self):
        return reverse("post-detail", args=(self.slug,))

    def save(self, *args, **kwargs):
        self.set_audit_fields()
        self.set_slug()
        super().save(*args, **kwargs)

    def set_audit_fields(self):
        now = timezone.now()
        if not self.id:
            self.created_at = now
        self.updated_at = now

    def set_slug(self):
        if not self.slug:
            all_emoji = list(emoji.EMOJI_DATA.keys())
            self.slug = "".join(random.choice(all_emoji) for _ in range(8))

    @property
    def reaction_summary(self) -> list:
        """
        Construct a list of tuples containing each distinct reaction text with
        it's count. Sort the list in descending order by count.
        """
        summary = dict()
        for reaction in self.reactions.all():
            summary[reaction.text] = summary.setdefault(reaction.text, 0) + 1
        return sorted(summary.items(), key=lambda x: x[1], reverse=True)

    def __str__(self):
        return self.text


class Reaction(Model):
    text = CharField(max_length=4, validators=[validate_emoji])
    created_at = DateTimeField(editable=False)
    updated_at = DateTimeField(editable=False)
    user = ForeignKey(get_user_model(), on_delete=CASCADE, related_name="reactions")
    post = ForeignKey(Post, on_delete=CASCADE, related_name="reactions")

    def get_absolute_url(self):
        return reverse("reaction-detail", args=(self.id,))

    def save(self, *args, **kwargs):
        self.set_audit_fields()
        super().save(*args, **kwargs)

    def set_audit_fields(self):
        now = timezone.now()
        if not self.id:
            self.created_at = now
        self.updated_at = now

    def __str__(self):
        return self.text
