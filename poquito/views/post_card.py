from poquito.views.post import PostDetail


class PostCard(PostDetail):
    template_name = "poquito/post_card.html"
