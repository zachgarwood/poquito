from django.contrib import messages
from django.contrib.auth.mixins import LoginRequiredMixin
from django.db.models import Prefetch
from django.urls import reverse
from django.views.generic.edit import CreateView, DeleteView
from django.views.generic.detail import DetailView

from poquito.models import Post, Reaction
from poquito.forms import ReactionForm


class CurrentUserFilterMixin:
    """Filter the view's queryset to the currently authed user"""

    def get_queryset(self):
        return self.model.objects.filter(user=self.request.user)


class ReactionCreate(LoginRequiredMixin, CreateView):
    form_class = ReactionForm
    model = Reaction

    def get(self, request, slug):
        self.post = Post.objects.get(slug=slug)
        return super().get(request, slug)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["post"] = self.post
        return context

    def post(self, request, slug):
        self.post = Post.objects.get(slug=slug)
        return super().post(request, slug)

    def form_valid(self, form):
        form.instance.user = self.request.user
        form.instance.post = self.post
        return super().form_valid(form)

    def get_success_url(self):
        messages.success(self.request, "🆕")
        return reverse("post-card", args=(self.post.slug,))


class ReactionDelete(CurrentUserFilterMixin, LoginRequiredMixin, DeleteView):
    model = Reaction

    def get(self, request, slug, pk):
        self.post = Post.objects.get(slug=slug)
        return super().get(request, slug, pk)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["post"] = self.post
        return context

    def post(self, request, slug, pk):
        self.post = Post.objects.get(slug=slug)
        return super().post(request, slug, pk)

    def get_success_url(self):
        messages.error(self.request, "🗑️")
        return reverse("post-card", args=(self.post.slug,))
