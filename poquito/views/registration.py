from django.contrib import messages
from django.contrib.auth import get_user_model
from django.contrib.auth.mixins import UserPassesTestMixin
from django.db import IntegrityError
from django.shortcuts import redirect
from mailauth.views import LoginView as EmailLoginView

EXISTING_USER_MESSAGE = "🤏 📧 🔗 ⏲️ 1️⃣5️⃣"


class CheckEmail(UserPassesTestMixin, EmailLoginView):
    def test_func(self):
        """Only show the signup form to non-authed users"""
        return not self.request.user.is_authenticated

    def handle_no_permission(self):
        """Redirect authed users to the Welcome page"""
        return redirect("post-list")

    def form_valid(self, form):
        """Create the user before sending the login link email"""
        messages.info(self.request, EXISTING_USER_MESSAGE)
        try:
            User = get_user_model()
            User.objects.create_user(form.cleaned_data[User.get_email_field_name()])
        except IntegrityError as exception:
            if "UNIQUE constraint failed" in exception.args[0]:
                messages.info(self.request, EXISTING_USER_MESSAGE)
            else:
                raise
        return super().form_valid(form)
