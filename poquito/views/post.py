from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib import messages
from django.contrib.messages.views import SuccessMessageMixin
from django.db.models import Prefetch
from django.urls import reverse_lazy
from django.views.generic.list import ListView
from django.views.generic.edit import CreateView, DeleteView, UpdateView
from django.views.generic.detail import DetailView

from poquito.models import Post, Reaction
from poquito.forms import PostForm


class CurrentUserFilterMixin:
    """Filter the view's queryset to the currently authed user"""

    def get_queryset(self):
        return self.model.objects.filter(user=self.request.user)


class AllPostList(ListView):
    model = Post

    def get_queryset(self):
        queryset = super().get_queryset()
        if self.request.user.is_authenticated:
            queryset = queryset.prefetch_related(
                Prefetch(
                    "reactions", to_attr="user_reactions", queryset=Reaction.objects.filter(user=self.request.user)
                )
            )
        return queryset.prefetch_related("reactions")


class PostList(CurrentUserFilterMixin, LoginRequiredMixin, ListView):
    model = Post

    def get_queryset(self):
        return super().get_queryset().prefetch_related("reactions")


class PostCreate(SuccessMessageMixin, LoginRequiredMixin, CreateView):
    form_class = PostForm
    model = Post
    success_message = "🆕"

    def form_valid(self, form):
        form.instance.user = self.request.user
        return super().form_valid(form)


class PostDetail(DetailView):
    model = Post

    def get_queryset(self):
        queryset = super().get_queryset()
        if self.request.user.is_authenticated:
            queryset = queryset.prefetch_related(
                Prefetch(
                    "reactions", to_attr="user_reactions", queryset=Reaction.objects.filter(user=self.request.user)
                )
            )

        return queryset.prefetch_related("reactions")


class PostUpdate(CurrentUserFilterMixin, LoginRequiredMixin, UpdateView):
    form_class = PostForm
    model = Post


class PostDelete(CurrentUserFilterMixin, LoginRequiredMixin, DeleteView):
    model = Post
    success_url = reverse_lazy("post-list")
