htmx.on("messages", (event) => {
    event.detail.value.forEach(createNotificationElement)
})
htmx.on("htmx:load", () => {
    setUpNotificationDeleteButtons()
})

function createNotificationElement(detail) {
    const template = htmx.find("#notification")
    const notification = htmx.find(template.content.cloneNode(true), ".notification")
    notification.innerHTML += detail.message
    notification.className += " " + detail.tags

    const notificationLayer = htmx.find("#notification-layer")
    notificationLayer.prepend(notification)

    setUpNotificationDeleteButtons()
}

function setUpNotificationDeleteButtons() {
    const deleteButtons = htmx.findAll(".notification>button.delete")
    deleteButtons.forEach((button) => {
        button.addEventListener("click", deleteNotification)
    })
}

function deleteNotification(event) {
    event.target.parentNode.remove()
}
