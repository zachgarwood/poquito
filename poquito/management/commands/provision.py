from pathlib import Path

from digitalocean import Domain, Droplet, Manager, SSHKey
from django.core.management.base import BaseCommand


class Command(BaseCommand):
    ENVIRONMENT = "production"
    PROJECT = "poquito"
    HOST = "zachgarwood.com"

    help = "Spins up a production server to host the application"

    @property
    def server_name(self):
        return f"{self.PROJECT}-{self.ENVIRONMENT}"

    @property
    def domain(self):
        return f"{self.PROJECT}.{self.HOST}"

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.droplet = None
        self.manager = Manager()
        self.ssh_keys = []

    def handle(self, *args, **options):
        self.stdout.write("Ensuring ssh keys...")
        self.ensure_ssh_keys()
        self.stdout.write("Ensuring host server...")
        self.ensure_server()
        self.stdout.write(f"Figure out how to ensure domain {self.domain}")
        self.stdout.write("Ensuring DNS records...")
        self.ensure_dns_records()

    def ensure_ssh_keys(self):
        self.ssh_keys = [key for key in self.manager.get_all_sshkeys() if key.name == self.PROJECT]
        if self.ssh_keys:
            self.stdout.write("✔️  Public ssh key exists")
            return
        public_key = Path().home().joinpath(".ssh", f"{self.PROJECT}.pub")
        self.stdout.write(f"Uploading public ssh key from {public_key}...")
        key = SSHKey(name=self.PROJECT, public_key=public_key.read_text())
        key.create()
        self.ssh_keys = [key]
        self.stdout.write("📤🔑  Public ssh key uploaded")

    def ensure_server(self):
        droplets = [droplet for droplet in self.manager.get_all_droplets() if droplet.name == self.server_name]
        if droplets:
            self.droplet = droplets[0]
            self.stdout.write(f"✔️  Server {self.server_name} exists")
            return
        self.stdout.write(f"Spinning up server {self.server_name}...")
        droplet = Droplet(
            name=self.server_name,
            region="sfo3",
            image="debian-11-x64",
            size_slug="s-1vcpu-1gb",
            ssh_keys=self.ssh_keys,
        )
        droplet.create()
        attempts = 4
        while attempts > 0:
            self.stdout.write(f"Checking status of {self.server_name}...")
            for action in droplet.get_actions():
                self.stdout.write(f"- {action.type}: {action.status}")
                if action.status == "completed":
                    self.droplet = droplet
                    self.stdout.write("💫🤖  Server spun up")
                    return
            attempts -= 1

    def ensure_dns_records(self):
        self.droplet.load()
        domain = Domain(name=f"{self.domain}")
        a_records = [record for record in domain.get_records() if record.type == "A" if record.name == "@"]
        if a_records:
            for record in a_records:
                if record.data != self.droplet.ip_address:
                    record.data = self.droplet.ip_address
                    record.save()
                    self.stdout.write(f"🆙🇦  A record for {self.domain} updated")
        else:
            domain.create_new_domain_record(
                type="A",
                name="@",
                data=self.droplet.ip_address,
                ttl=3600,
            )
            self.stdout.write(f"🆕🇦  A record for {self.domain} created")
        self.stdout.write("✔️  A records exist")
