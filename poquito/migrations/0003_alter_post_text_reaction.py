# Generated by Django 4.0 on 2022-07-18 00:46

from django.db import migrations, models
import django.db.models.deletion
import poquito.models


class Migration(migrations.Migration):

    dependencies = [
        ("mailauth_user", "0005_emailuser_email_hash_alter_emailuser_email"),
        ("poquito", "0002_add_slug_to_post"),
    ]

    operations = [
        migrations.AlterField(
            model_name="post",
            name="text",
            field=models.CharField(
                max_length=255, validators=[poquito.models.validate_emoji, poquito.models.validate_linebreaks]
            ),
        ),
        migrations.CreateModel(
            name="Reaction",
            fields=[
                ("id", models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name="ID")),
                ("text", models.CharField(max_length=4, validators=[poquito.models.validate_emoji])),
                ("created_at", models.DateTimeField(editable=False)),
                ("updated_at", models.DateTimeField(editable=False)),
                (
                    "post",
                    models.ForeignKey(
                        on_delete=django.db.models.deletion.CASCADE, related_name="reactions", to="poquito.post"
                    ),
                ),
                (
                    "user",
                    models.ForeignKey(
                        on_delete=django.db.models.deletion.CASCADE,
                        related_name="reactions",
                        to="mailauth_user.emailuser",
                    ),
                ),
            ],
        ),
    ]
